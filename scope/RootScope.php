<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body ng-app="myApp">
		<p>The rootScope's favorite color:</p>
		<h1>{{color}}</h1>

		<div ng-controller="myCtrl">
			<p>The scope of the controller's favorite color:</p>
			<h1>{{color}}</h1>
		</div>

		<p>The rootScope's favorite color is still:</p>
		<h1>{{color}}</h>

		<script>
			var app = angular.module('myApp', []);

			//root scope
			app.run(function($rootScope) 
			{
				$rootScope.color = 'blue';
			});

			//scope baru
			app.controller('myCtrl', function($scope) 
			{
				$scope.color = "red";
				// diluar scope myCtrl warna mengikuti rootScope
			});
		</script>
	</body>
</html>