<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="myCtrl">
			<p>The time is:</p>
			<h1>{{theTime}}</h1>
		</div>
		
		<script>
			var app = angular.module('myApp', []);
			
			// interval adalah versi AngularJS dari fungsi window.setInterval
			app.controller('myCtrl', function($scope, $interval) 
			{
				$scope.theTime = new Date().toLocaleTimeString();
				
				$interval(function () 
				{
					$scope.theTime = new Date().toLocaleTimeString();
				}, 1000);
			});
		</script>
	</body>
</html>