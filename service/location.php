<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="myCtrl">
			<p>The url of this page is:</p>
			<h3>{{myUrl}}</h3>
		</div>
		<script>
			var app = angular.module('myApp', []);
			
			// service $location.absUrl(); digunakan untuk mendapatkan absolute url
			// location dilewatkan ke controller sebagai argumen. Untuk menggunakan layanan di controller, itu harus didefinisikan sebagai depedency.
			app.controller('myCtrl', function($scope, $location) 
			{
				$scope.myUrl = $location.absUrl();
			});
		</script>
	</body>
</html>