<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="myCtrl">
			<p>The hexadecimal value of 255 is:</p>
			<h1>{{hex}}</h1>
		</div>
		
		<script>
			var app = angular.module('myApp', []);
			
			// membuat service
			app.service('hexafy', function() 
			{
				this.myFunc = function (x) {
					return x.toString(16);
				}
			});

			app.controller('myCtrl', function($scope, hexafy) {
				$scope.hex = hexafy.myFunc(255);
			});
		</script>
	</body>
</html>