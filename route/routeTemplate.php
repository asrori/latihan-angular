<!DOCTYPE html>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular-route.js"></script>
	</head>
	<body ng-app="myApp">
		<p><a href="#/!">Main</a></p>
		<a href="#!banana">Banana</a>
		<a href="#!tomato">Tomato</a>
		<hr/>
		<div ng-view></div>

		<script>
			var app = angular.module("myApp", ["ngRoute"]);
			app.config(function($routeProvider) {
				$routeProvider
				// .when("/", {
				// 	template : "<h1>Main</h1><p>Click on the links to change this content</p>"
				// })
				.when("/banana", {
					template : "<h1>Banana</h1><p>Bananas contain around 75% water.</p>"
				})
				.when("/tomato", {
					template : "<h1>Tomato</h1><p>Tomatoes contain around 95% water.</p>"
				})
				.otherwise({
					template : "<h1>None</h1><p>Nothing has been selected</p>"
				});
			});
		</script>
	</body>
</html>