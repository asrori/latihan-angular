<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>

		<style>
			input.ng-invalid {
				background-color:pink;
			}

			input.ng-valid {
				background-color:lightgreen;
			}

			form.ng-pristine {
				background-color:lightblue;
			}

			form.ng-dirty {
				background-color:pink;
			}
		</style>
	</head>
	<body ng-app="">
		<form name="myForm">
			<p>Try writing in the input field:</p>
			<input name="myName" ng-model="myName" required><br/>
			<p>Tes</p>
		</form>

	</body>
</html>