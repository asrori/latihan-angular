<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body ng-app="">
		<form name="myForm">
			<p>Name:
				<input name="myName" ng-model="myName" required>
				<span ng-show="myForm.myName.$touched && myForm.myName.$invalid">The name is required.</span>
			</p>

			<p>Address:
				<input name="myAddress" ng-model="myAddress" required>
			</p>
		</form>
	</body>
</html>