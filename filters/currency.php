<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="costCtrl">
			<!-- filter currency memformat number menjadi currency -->
			<h1>Price: {{ price | currency }}</h1>
		</div>

		<script>
			var app = angular.module('myApp', []);
			app.controller('costCtrl', function($scope) {
				$scope.price = 58;
			});
		</script>
	</body>
</html>