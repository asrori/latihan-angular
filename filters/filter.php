<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="namesCtrl">
			<!-- filter digunakan untuk memfilter kata di sebuah array -->
			<!-- filter hanya dapat digunakan di array, dan mengembalikan array yang cocok dengan filter yg bersangkutan -->
			<ul>
				<li ng-repeat="x in names | filter : 'i'">
					{{ x }}
				</li>
			</ul>

			<!-- kombinasi filter dengan model -->
			<p><input type="text" ng-model="test"></p>
			<ul>
				<li ng-repeat="x in names | filter:test">
					{{ x }}
				</li>
			</ul>

		</div>

		<script>
			angular.module('myApp', []).controller('namesCtrl', function($scope) {
				$scope.names = [
					'Jani',
					'Carl',
					'Margareth',
					'Hege',
					'Joe',
					'Gustav',
					'Birgit',
					'Mary',
					'Kai'
				];
			});
		</script>
	</body>
</html>