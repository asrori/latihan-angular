<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="namesCtrl">
			<ul ng-app="myApp" ng-controller="namesCtrl">
				<li ng-repeat="x in names">
					{{x | myFormat}}
				</li>
			</ul>
		</div>

		<script>
			var app = angular.module('myApp', []);

			// memformat ke uppercase ketika bilangan genap
			app.filter('myFormat', function() {
				return function(x) {
					var i, c, txt = "";
					for (i = 0; i < x.length; i++) 
					{
						c = x[i];
						if (i % 2 == 0) {
							c = c.toUpperCase();
						}

						txt += c;
					}

					return txt;
				};
			});

			app.controller('namesCtrl', function($scope) {
				$scope.names = ['Jani', 'Carl', 'Margareth', 'Hege', 'Joe', 'Gustav', 'Birgit', 'Mary', 'Kai'];
			});
		</script>
	</body>
</html>