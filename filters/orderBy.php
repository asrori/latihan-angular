<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="namesCtrl">
			<p>Looping with objects:</p>
			<ul>
				<!-- filter orderBy mengurutkan ascending -->
				<li ng-repeat="x in names | orderBy:'country'">
					{{ x.name + ', ' + x.country }}
				</li>
			</ul>
		</div>

		<script>
			angular.module('myApp', []).controller('namesCtrl', function($scope) {
				$scope.names = [
					{name : 'Jani', country : 'Norway'},
					{name : 'Carl', country : 'Sweden'},
					{name : 'Margareth', country : 'England'},
					{name : 'Hege', country : 'Norway'},
					{name : 'Joe', country : 'Denmark'},
					{name : 'Gustav', country : 'Sweden'},
					{name : 'Birgit', country : 'Denmark'},
					{name : 'Mary', country : 'England'},
					{name : 'Kai', country : 'Norway'}
				];
			});
		</script>
	</body>
</html>