<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="personCtrl">
			<!-- filter uppercase menjadikan huruf kecil -->
			<p>The name is {{ lastName | lowercase }}</p>
		</div>

		<script>
			angular.module('myApp', []).controller('personCtrl', function($scope) 
			{
				$scope.firstName = "John",
				$scope.lastName  = "Doe"
			});
		</script>
	</body>
</html>