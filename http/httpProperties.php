<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="myCtrl"> 
			<p>Config : {{config}}</p>
			<p>Data : {{content}}</p>
			<p>Headers : {{headers}}</p>
			<p>Status : {{statuscode}}</p>
			<p>StatusText : {{statustext}}</p>
		</div>
		
		<script>
			var app = angular.module('myApp', []);

			/*
				Respon dari server adalah objek dengan properti sbg berikut :
				.config objek yang digunakan untuk menghasilkan request
				.data sebuah string, atau objek, membawa respon dari server
				.headers berfungsi untuk menggunakan informasi header
				.status nomor yang menentukan status HTTP
				.statusText string yang mendefinisikan status HTTP
			 */
			
			app.controller('myCtrl', function($scope, $http) 
			{
				$http.get("welcome.htm")
				.then(function(response) {
					$scope.config     = response.config;
					$scope.content    = response.data;
					$scope.headers    = response.headers;
					$scope.statuscode = response.status;
					$scope.statustext = response.statustext; 
				});
			});
		</script>
	</body>
</html>