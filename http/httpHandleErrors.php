<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="myCtrl"> 
			<h1>{{content}}</h1>
		</div>
		
		<script>
			var app = angular.module('myApp', []);
			app.controller('myCtrl', function($scope, $http) 
			{
				$http.get("wrongfilename.htm").then(function(response) 
				{
					//First function handles success
					$scope.content = response.data;
				}, 
				function(response) 
				{
					//Second function handles error
					$scope.content = "Something went wrong";
				});
			});
		</script>
	</body>
</html>