<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="myCtrl"> 
			<p>Today's welcome message is:</p>
			<h1>{{myWelcome}}</h1>
		</div>
		
		<script>
			var app = angular.module('myApp', []);
			
			/* 
			 	ada beberapa jenis method di http seervice diantaranya :
				.delete()
				.get()
				.head()
				.jsonp()
				.patch()
				.post()
				.put()
			*/
			
			app.controller('myCtrl', function($scope, $http) {
				$http({
					method : "GET",
					url    : "welcome.htm"
				}).then(function mySuccess(response) {
					$scope.myWelcome = response.data;
				}, function myError(response) {
					$scope.myWelcome = response.statusText;
				});
			});
		</script>
	</body>
</html>