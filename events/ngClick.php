<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<div ng-app="myApp" ng-controller="myCtrl">
			<button ng-click="count = count + 1">Click Me!</button>
			<button ng-click="myFunction()">Click Me!</button>
			<p>{{ count }}</p>
		</div>

		<script>
			var app = angular.module('myApp', []);
			app.controller('myCtrl', function($scope) {
				$scope.count = 0;
				$scope.myFunction = function() {
					$scope.count++;
				}
			});
		</script> 
	</body>
</html>