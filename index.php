<!DOCTYPE html>
<html>
	<head>
		<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	</head>
	<body>
		<?php
		/**
		 * ng-app   : angular ditandai dengan tag ini
		 * ng-init  : mendiskripsikan sebagai sebuah variable di angular
		 * ng-model : mendiskripsikan sebuah data
		 * ng-bind  : mendiskripsikan hasil dari sebuah data yg diambil
		 * {{ .. }} : adalah sebuah expresi di angular
		 */
		?>
		<div ng-app="" ng-init="firstName='Asrori'">
			<p>Masukan Nama Anda ::</p>
			<p><input type="text" ng-model="name"></p>
			<p ng-bind="name"></p>

			<p>Namaku adalah :: <span ng-bind="firstName"></span></p>
			<p>Contoh Expression 5+5 : {{ 5 + 5 }}</p>
		</div>
		<br/>
	</body>
</html>