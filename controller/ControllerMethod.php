<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<?php
		/**
		 * ng-controller : menandakan sebuah controller di sebuah module
		 */
		?>
		<div ng-app="myApp" ng-controller="personCtrl">
			First Name: <input type="text" ng-model="firstName"><br><br/>
			Last Name: <input type="text" ng-model="lastName"><br>
			<br>
			Full Name: {{fullName()}}
		</div>

		<script>
			// membuat sebuah module
			var app = angular.module('myApp', []);

			// menjalankan method di controller
			app.controller('personCtrl', function($scope) 
			{
				$scope.firstName = "John";
				$scope.lastName  = "Doe";
				$scope.fullName  = function() {
					return $scope.firstName + " " + $scope.lastName;
				};
			});
		</script>
	</body>
</html>