<!DOCTYPE html>
<html>
	<head>
		<script src="../assets/angular.min.js"></script>
	</head>
	<body>
		<?php
		/**
		 * ng-controller : menandakan sebuah controller di sebuah module
		 */
		?>
		<p>Try to change the names.</p>
		<div ng-app="myApp" ng-controller="myCtrl">
			First Name: <input type="text" ng-model="firstName"><br><br>
			Last Name: <input type="text" ng-model="lastName"><br>
			<br>
			Full Name: {{firstName + " " + lastName}}
		</div>

		<script>
			// membuat sebuah module
			var app = angular.module('myApp', []);

			// menjalankan controller
			app.controller('myCtrl', function($scope) 
			{
    			$scope.firstName = "John";
    			$scope.lastName  = "Doe";
			});
		</script>
	</body>
</html>