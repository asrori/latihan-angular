<!DOCTYPE html>
<html>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<body ng-app="myApp">
		<?php
		/**
		 * sebuah directive : atau membuat sebuah tag yang dapat di cetak dimanapun sesuai kebutuhan
		 * nama directive harus camel case
		 * namun untuk memanggilnya harus dipisah dengan tag -
		 */
		?>

		<?php
		/**
		 * tag-tag restrict
		 * E for Element name
		 * A for Attribute
		 * C for Class
		 * M for Comment
		 */
		?>

		<?php //sebuah tag ?>
		<w3-test-directive></w3-test-directive>

		<?php //didalam div ?>
		<div w3-test-directive></div>

		<?php //didalam class ?>
		<div class="w3-test-directive-class"></div>

		<?php //sebuah comment ?>
		<!-- directive: w3-test-directive-comment -->

		<script>
			var app = angular.module("myApp", []);

			app.directive("w3TestDirective", function() {
				return {
					template : "<h1>Made by a directive!</h1>"
				};
			});

			// didalam class
			app.directive("w3TestDirectiveClass", function() {
				return {
					restrict : "C",
					template : "<h1>Made by a directive Class!</h1>"
				};
			});

			// di comment
			app.directive("w3TestDirectiveComment", function() {
				return {
					restrict : "M",
					replace : true,
					template : "<h1>Made by a directive Comment!</h1>"
				};
			});
		</script>

	</body>
</html>