<!DOCTYPE html>
<html>
	<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
	<body>
		<form ng-app="" name="myForm" ng-init="myText = 'post@myweb.com'">
			Masukan Email :
			<input type="email" name="myEmail" ng-model="myText" required>
			<p>Valid: {{myForm.myEmail.$valid}}</p>
			<p>Dirty: {{myForm.myEmail.$dirty}}</p>
			<p>Touched: {{myForm.myEmail.$touched}}</p>
		</form>

	</body>
</html>
